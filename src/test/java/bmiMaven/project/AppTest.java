package bmiMaven.project;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import Bmi.Project.BmiCalculator;
import Bmi.Project.BmiCalculatorImpl;
import Bmi.Project.BmiInputDataCheck;
import Bmi.Project.MyException;

/**
 * Unit test for simple App.
 */
public class AppTest {

	BmiCalculator calculatorObj = new BmiCalculatorImpl();
	BmiInputDataCheck inputObj = new BmiInputDataCheck();
	
	@Test
	public void testBmiCalculatorsiBmi() {
		assertEquals(60, calculatorObj.siBmi(60,1),0);
		assertEquals(49.59, calculatorObj.siBmi(60,1.1),5);
		assertEquals(27.70, calculatorObj.siBmi(100,1.9),5);
		assertEquals(24, calculatorObj.siBmi(54,1.5),0);
		assertEquals(0, calculatorObj.siBmi(0,1.5),0);
		assertEquals(400, calculatorObj.siBmi(1000000,50),0);
		System.out.println("-----siBmi Test succesful!-----");
	}
	@Test
	public void testBmiCalculatorusBmi() {
		assertEquals(14.91, calculatorObj.usBmi(110,6),2);
		assertEquals(29.8341, calculatorObj.usBmi(220,6),3);
		assertEquals(33.1972, calculatorObj.usBmi(170,5),3);
		assertEquals(24.4097, calculatorObj.usBmi(80,4),3);
		assertEquals(0, calculatorObj.usBmi(0,4),3);
		assertEquals(149999813, calculatorObj.usBmi(122901696,2),0);
		System.out.println("-----usBmi Test successful!-----");
	}
	@Test
	public void testBmiCalculatorscanBmi(){
		assertEquals("Normal",calculatorObj.scanBmi(24));
		assertEquals("Obese Class I.",calculatorObj.scanBmi(31));
		assertEquals("Severe Thinness",calculatorObj.scanBmi(15));
		System.out.println("-----scanBmi Test succesful!-----");
	}

	@Test
	public void testBmiInputDataCheckinputWeightDataCheckSi() throws MyException{
		assertEquals(25, inputObj.inputWeightDataCheckSi(6,25),0);
		assertEquals(50, inputObj.inputWeightDataCheckSi(9,5000),0);
		assertEquals(30, inputObj.inputWeightDataCheckSi(25,30000),0);
		assertEquals(280, inputObj.inputWeightDataCheckSi(25,280000),0);
		System.out.println("-----inputWeightDataCheck Test succesful!-----");
	}
	@Test
	public void testBmiInputDataCheckinputWeightDataCheckUs() throws MyException{
		assertEquals(55, inputObj.inputWeightDataCheckUs(3,55),0);
		assertEquals(10, inputObj.inputWeightDataCheckUs(7,160),0);
		assertEquals(100, inputObj.inputWeightDataCheckUs(30,1600),0);
		assertEquals(650, inputObj.inputWeightDataCheckUs(30,10400),0);
		System.out.println("-----inputWeightDataCheck Test succesful!-----");
	}	
	@Test
	public void testBmiInputDataCheckinputHeightDataCheckSi() throws MyException{
		assertEquals(1.5, inputObj.inputHeightDataCheckSi(150),2);
		assertEquals(2, inputObj.inputHeightDataCheckSi(2),0);
		assertEquals(2.54, inputObj.inputHeightDataCheckSi(2540),3);
		System.out.println("-----inputWeightDataCheck Test succesful!-----");
		
	}
	@Test
	public void testBmiInputDataCheckinputHeightDataCheckUs() throws MyException{
		assertEquals(5.62, inputObj.inputHeightDataCheckUs(5.62),2);
		assertEquals(5, inputObj.inputHeightDataCheckUs(60),0);
		assertEquals(9.5, inputObj.inputHeightDataCheckUs(114),0);
		System.out.println("-----inputWeightDataCheck Test succesful!-----");
		
	}	

}