package Bmi.Project;

public class App 
{
    public static void main( String[] args )
    {
		BmiInputDataCheck inputObj = new BmiInputDataCheck();
		BmiCalculator calculatorObj = new BmiCalculatorImpl();
		String unit = inputObj.inputUnit();
		try{
		if (unit.equals("US") || unit.equals("SI")) {
			double weight = inputObj.inputWeight();			
			double height = inputObj.inputHeight();
			double bMi;
			switch (unit) {
			case "US":
				bMi = calculatorObj.usBmi(weight, height);
				System.out.println("Your BMI value:" + bMi);
				System.out.println("Your physique:" + calculatorObj.scanBmi(bMi));
				break;
			case "SI":
				bMi = calculatorObj.siBmi(weight, height);
				System.out.println("Your BMI value:" + bMi);
				System.out.println("Your physique:" + calculatorObj.scanBmi(bMi));
				break;
				}
			} else {
					System.out.println("Incorrect unit!");
			}
				
		}
		catch (MyException e){
				System.out.println(e);
			}    
    }
}
