package Bmi.Project;

import java.util.Scanner;

public class BmiInputDataCheck {
	private double age;
	private String unit;
	Scanner input = new Scanner(System.in);

	public String inputUnit() {
		System.out.println("Which unit of measurement you want to use us (pounds, foot) or si (kilograms, meters?");
		unit = input.nextLine();
		unit = unit.toUpperCase();
		return unit;
	}

	public double inputAge() throws MyException {
		System.out.println("Enter your age:");
		age = inputDataCheck();
		if (age < 2) {
			throw new MyException("The calculator 2 years and older can use!");
		}
		return age;
	}
	
	public double inputWeight() throws MyException {
		age = inputAge();
		System.out.println("Enter your weight:");
		double weight = inputDataCheck();

		switch (unit) {
		case "SI":
			weight = inputWeightDataCheckSi (age, weight);
			break;

		case "US":
			weight = inputWeightDataCheckUs(age, weight);
			break;

		}
		return weight; // kg or pound
	}

	public double inputWeightDataCheckSi(double age, double weight) throws MyException {
		if (weight < 5) {
			throw new MyException("Not valid weight value!");
		}
		if (age < 8 && weight > 30) { // 8 years old child weight max 29.99kg
			if (weight >= 300 && weight <= 2999) {
				weight = weight / 100;
			} else if (weight > 2999 && weight <= 29990) {
				weight = weight / 1000;
			} else {
				throw new MyException("Not valid weight value!");
			}
		}
		if (age >= 8 && weight > 299.99) {// people max weight 299.99 kg
			if (weight >= 3000 && weight <= 29999) {
				weight = weight / 100;
			} else if (weight > 29999 && weight <= 299990) {
				weight = weight / 1000;
			} else {
				throw new MyException("Not valid weight value!");
			}
		}
		return weight;
	}

	public double inputWeightDataCheckUs(double age, double weight) throws MyException {
		if (weight < 11) {
			throw new MyException("Not valid weight value!");
		}
		if (age < 8 && weight > 66) {
			if (weight >= 105.82 && weight <= 1057.86) {
				weight = weight / 16;
			} else {
				throw new MyException("Not valid weight value!");
			}
		}
		if (age >= 8 && weight > 661) {
			if (weight >= 1072 && weight <= 10546) {
				weight = weight / 16;
			} else {
				throw new MyException("Not valid weight value!");
			}
		}
		return weight;
	}

	public double inputHeight() throws MyException {
		System.out.println("Enter your height:");
		double height = inputDataCheck();// character check
		switch (unit) {
		case "SI":
			height = inputHeightDataCheckSi(height);
			break;
		case "US":
			height = inputHeightDataCheckUs(height);
			break;
		}
		return height; // meter or foot
	}
	
	public double inputHeightDataCheckSi(double height) throws MyException {
		if (height <0.5){
			throw new MyException("Value too low!");
		}
		if (height > 3) {
			if (height >= 3.1 && height <= 30) {
				height = height / 10;
			} else if (height > 30 && height <= 300) {
				height = height / 100;
			} else if (height > 300 && height <= 3000) {
				height = height / 1000;
			} else {
				throw new MyException("Not valid height value!");
			}
		}
		return height;
	}
	
	public double inputHeightDataCheckUs(double height) throws MyException {
		if (height <1.6){
			throw new MyException("Value too low!");
		}
		if (height > 9.84) {
			if (height >= 12.2 && height <= 118.11) {
				height = height / 12;
			} else {
				throw new MyException("Not valid height value!");
			}
		}
		return height;
	}
	
	public double inputDataCheck() throws MyException {
		String text = input.nextLine();
		int numberOfDot = 0;
		for (int i = 0; i < text.length(); i++) {
			char testChar = text.charAt(i);
			boolean testResult = Character.isDigit(testChar) || testChar == ('.');
			if (testChar == ('.')) {
				numberOfDot++;
			}
			if (!testResult || numberOfDot > 1) {
				throw new MyException("Not correct data!");
			}
		}
		double inputData = Double.parseDouble(text);
		return inputData;
	}

}

