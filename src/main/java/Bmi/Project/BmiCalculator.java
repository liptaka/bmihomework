package Bmi.Project;

public interface BmiCalculator {

	public double usBmi(double weight, double height);
	public double siBmi(double weight, double height);
	public String scanBmi(double bmi);
}
